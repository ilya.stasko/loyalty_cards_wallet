package com.example.loyaltycardwallet.ui.add

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.loyaltycardwallet.R

class AddCardActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_card)
    }
}