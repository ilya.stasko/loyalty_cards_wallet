package com.example.loyaltycardwallet.ui.add

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.loyaltycardwallet.data.CardRepository
import com.example.loyaltycardwallet.model.InputCard
import com.example.loyaltycardwallet.ui.add.default_cards.DefaultCardHeaderItem
import com.example.loyaltycardwallet.ui.add.default_cards.DefaultCardItem
import kotlinx.coroutines.launch

class DefaultCardsViewModel(private val repository: CardRepository) : ViewModel() {

    private val _cards = MutableLiveData<List<DefaultCardItem>>()
    val defaultCards = _cards

    init {
        getInputCards()
    }

    private fun getInputCards() {
        viewModelScope.launch {
            val cards = repository.getCardsInfo()
            var header = DefaultCardHeaderItem(cards[0].shopName[0].toString())
            val defaultCards: List<DefaultCardItem> = cards.mapIndexed { i: Int, defaultCard: InputCard ->
                if (i == 0) {
                    DefaultCardItem(defaultCard.shopName, defaultCard.cardName, defaultCard.color, header)
                } else {
                    val prevCard = cards[i - 1]
                    if (defaultCard.shopName[0].isLetter()) {
                        if (!prevCard.shopName[0].equals(defaultCard.shopName[0], true)) {
                            header = DefaultCardHeaderItem(defaultCard.shopName[0].toString().uppercase())
                            DefaultCardItem(defaultCard.shopName, defaultCard.cardName, defaultCard.color, header)
                        } else {
                            DefaultCardItem(defaultCard.shopName, defaultCard.cardName, defaultCard.color, header)
                        }
                    } else {
                        if (prevCard.shopName[0].isLetter()) {
                            header = DefaultCardHeaderItem("#")
                            DefaultCardItem(defaultCard.shopName, defaultCard.cardName, defaultCard.color, header)
                        } else {
                            DefaultCardItem(defaultCard.shopName, defaultCard.cardName, defaultCard.color, header)
                        }
                    }
                }
            }
            _cards.value = defaultCards
        }
    }
}