package com.example.loyaltycardwallet.ui.add.default_cards

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.loyaltycardwallet.R
import com.example.loyaltycardwallet.databinding.ListItemDefaultCardsHeaderBinding
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractHeaderItem
import eu.davidea.flexibleadapter.items.IFlexible
import eu.davidea.viewholders.FlexibleViewHolder

class DefaultCardHeaderItem(private val title: String) :
    AbstractHeaderItem<DefaultCardHeaderItem.HeaderViewHolder>() {

    override fun equals(other: Any?): Boolean {
        if (other is DefaultCardHeaderItem) {
            return title.equals(other.title, true)
        }
        return false
    }

    override fun hashCode(): Int {
        return title.hashCode()
    }

    override fun toString(): String = title

    override fun getLayoutRes(): Int = R.layout.list_item_default_cards_header

    override fun createViewHolder(view: View, adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>): HeaderViewHolder {
        val binding = ListItemDefaultCardsHeaderBinding.bind(view)
        return HeaderViewHolder(binding, adapter)
    }

    override fun bindViewHolder(adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>, holder: HeaderViewHolder, position: Int, payloads: MutableList<Any>?) {
            holder.binding.title.text = title
    }

    inner class HeaderViewHolder(
        val binding: ListItemDefaultCardsHeaderBinding,
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>
    ) : FlexibleViewHolder(binding.root, adapter, true)
}