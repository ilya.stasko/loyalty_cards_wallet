package com.example.loyaltycardwallet.ui.add.default_cards

import android.graphics.Color
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.amulyakhare.textdrawable.TextDrawable
import com.example.loyaltycardwallet.R
import com.example.loyaltycardwallet.databinding.ListItemDefaultCardsBinding
import com.example.loyaltycardwallet.utils.countWords
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractSectionableItem
import eu.davidea.flexibleadapter.items.IFilterable
import eu.davidea.flexibleadapter.items.IFlexible
import eu.davidea.flexibleadapter.utils.FlexibleUtils
import eu.davidea.viewholders.FlexibleViewHolder
import java.util.Objects

class DefaultCardItem(
    val shopName: String,
    val cardName: String,
    val color: String,
    header: DefaultCardHeaderItem,
) : AbstractSectionableItem<DefaultCardItem.InputCardViewHolder, DefaultCardHeaderItem>(header), IFilterable<String> {

    init {
        isDraggable = false
        isSwipeable = false
    }

    override fun equals(other: Any?): Boolean {
        if (other is DefaultCardItem) {
            return other.shopName == shopName && other.cardName == cardName && other.color == color
        }
        return false
    }

    override fun hashCode(): Int {
        return Objects.hash(shopName, cardName, color)
    }

    override fun toString(): String {
        return "$shopName, $cardName, $color"
    }

    override fun getLayoutRes(): Int = R.layout.list_item_default_cards

    override fun createViewHolder(view: View, adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>): InputCardViewHolder {
        val binding = ListItemDefaultCardsBinding.bind(view)
        return InputCardViewHolder(binding, adapter)
    }

    override fun bindViewHolder(adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>, holder: InputCardViewHolder, position: Int, payloads: MutableList<Any>?) {
        if (adapter.hasFilter()) {
            val filter = adapter.getFilter(String::class.java)
            FlexibleUtils.highlightText(holder.binding.shopNameTextView, shopName, filter)
        } else {
            holder.binding.shopNameTextView.text = shopName
        }
        val roundDrawable = TextDrawable.builder()
            .beginConfig()
            .bold()
            .endConfig()
            .buildRound(getFirstLettersFromShopName(), Color.parseColor(color))
        holder.binding.cardRoundImageView.setImageDrawable(roundDrawable)
    }

    override fun filter(constraint: String): Boolean {
        for(word in constraint.split(FlexibleUtils.SPLIT_EXPRESSION)) {
            if (shopName.contains(word, true))
                return true
        }
        return false
    }

    // возвращает строку для помещения в аватар карты
    // если название магазина состоит из 2х и более слов,
    // берем первые буквы первых двух слов,
    // если название магазина состоит из 1 слова,
    // возаращем 1ую букву названия
    private fun getFirstLettersFromShopName(): String {
        // создает строку из букв, содержащихся в названии магазина
        // для отображения в "аватаре" карты
        val strBuilder = StringBuilder(2)
        // определяем количество слов в названии магазина
        val cnt = shopName.countWords()
        strBuilder.append(shopName[0])
        // Если слов в названии магазина > 1, то берем первые буквы первых 2х слов
        // иначе берем первые 2 буквы названия
        var i = 1
        if (cnt > 1) {
            while (shopName[i] != ' ') i++
            strBuilder.append(shopName[i + 1])
        } else {
            // пока не наткнемся на цифру или букву
            while (!(shopName[i].isLetterOrDigit())) i++
            strBuilder.append(shopName[i])
        }
        return strBuilder.toString()
    }

    inner class InputCardViewHolder(
        val binding: ListItemDefaultCardsBinding,
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>
    ) : FlexibleViewHolder(binding.root, adapter)
}