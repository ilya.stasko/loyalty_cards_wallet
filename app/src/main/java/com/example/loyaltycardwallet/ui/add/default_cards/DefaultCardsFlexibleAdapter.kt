package com.example.loyaltycardwallet.ui.add.default_cards

import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem

class DefaultCardsFlexibleAdapter(cards: List<AbstractFlexibleItem<*>>) : FlexibleAdapter<AbstractFlexibleItem<*>>(cards) {

    override fun onCreateBubbleText(position: Int): String {
        val iFlexible = getItem(position)
        val text = iFlexible.toString()[0]
        return if (text.isLetter()) {
            text.toString()
        } else {
            "#"
        }
    }
}