package com.example.loyaltycardwallet.ui.add.default_cards

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.loyaltycardwallet.App
import com.example.loyaltycardwallet.R
import com.example.loyaltycardwallet.databinding.FragmentDefaultCardsBinding
import com.example.loyaltycardwallet.di.AppComponent
import com.example.loyaltycardwallet.ui.add.DefaultCardsViewModel
import com.example.loyaltycardwallet.utils.hideKeyboard
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.common.SmoothScrollLinearLayoutManager

class DefaultCardsFragment : Fragment() {

    private lateinit var appComponent: AppComponent
    private lateinit var defaultCardsViewModel: DefaultCardsViewModel

    private val defaultCardsAdapter = DefaultCardsFlexibleAdapter(listOf())
    private var _binding: FragmentDefaultCardsBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentDefaultCardsBinding.inflate(inflater, container, false)

        appComponent = (requireActivity().application as App).appComponent
        val factory = appComponent.getDefaultCardsViewModelFactory()
        defaultCardsViewModel = ViewModelProvider(this, factory)[DefaultCardsViewModel::class.java]

        initUi()
        subscribeUi()
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun initUi() {
        initSearchView()
        with(binding) {
            defaultCardsRecyclerView.layoutManager = SmoothScrollLinearLayoutManager(requireActivity())
            defaultCardsRecyclerView.adapter = defaultCardsAdapter
            defaultCardsToolbar.setNavigationOnClickListener {
                Toast.makeText(requireContext(), "click on back button", Toast.LENGTH_SHORT).show()
            }
            binding.addNewCardButton.setOnClickListener {
                Toast.makeText(requireContext(), "click on fab", Toast.LENGTH_SHORT).show()
            }
        }
        with(defaultCardsAdapter) {
            setDisplayHeadersAtStartUp(true)
            setStickyHeaders(true)
            showAllHeaders()
            addListener(object : FlexibleAdapter.OnItemClickListener {
                override fun onItemClick(view: View?, position: Int): Boolean {
                    val item = defaultCardsAdapter.getItem(position)
                    if (item is DefaultCardItem) {
                        Toast.makeText(requireContext(), "shop = ${item.shopName}", Toast.LENGTH_SHORT).show()
                    }
                    return false
                }
            })
        }
        val fastScroller = binding.fastScroller.apply {
            setViewsToUse(R.layout.fast_scroller_layout, R.id.fast_scroller_bubble, R.id.fast_scroller_handle)
            addOnScrollStateChangeListener { scrolling: Boolean ->
                if (scrolling) binding.addNewCardButton.hide()
                else binding.addNewCardButton.show()
            }
        }
        defaultCardsAdapter.fastScroller = fastScroller
    }

    private fun initSearchView() {
        val searchMenuItem = binding.defaultCardsToolbar.menu.findItem(R.id.action_search)
        val searchView = searchMenuItem.actionView as SearchView
        searchView.queryHint = getString(R.string.hint_search_shop)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                hideKeyboard(requireContext(), searchView)
                searchView.clearFocus()
                return onQueryTextChange(query)
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (defaultCardsAdapter.hasNewFilter(newText)) {
                    defaultCardsAdapter.setFilter(newText)
                    defaultCardsAdapter.filterItems(200L)
                }
                return true
            }
        })
    }

    private fun subscribeUi() {
        defaultCardsViewModel.defaultCards.observe(viewLifecycleOwner) { cards: List<DefaultCardItem> ->
            defaultCardsAdapter.updateDataSet(cards, true)
        }
    }
}