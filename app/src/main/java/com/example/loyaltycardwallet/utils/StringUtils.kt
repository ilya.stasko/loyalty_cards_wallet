package com.example.loyaltycardwallet.utils

fun String.countWords(): Int =
    if (this.isEmpty()) {
        0
    } else {
        val words: Array<String> = this.split("\\s+".toRegex()).toTypedArray()
        words.size
    }